import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Rate } from 'src/rate';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  private baseCurrency = '';
  private toCurrency = '';
  private rates = new Array();

  constructor(private http: HttpClient) { }

  public getBaseCurrency (): string {
    return this.baseCurrency;
  }

  public setToCurrency (value: string) {
    this.toCurrency = value;
  }

  public getRates() {
    if(this.rates.length === 0) {
      this.getRatesFromAPI();
    }
    return this.rates;
  }

  public convert(amount : number): number {
    let factor: number;
    factor = this.getRate();
    return amount * factor;
  }

  private getRate(): number {
    let result = 1;
    this.getRates();
    this.rates.forEach(value => {
      if(value.currency === this.toCurrency){
        result = value.exchangeRate; 
      }
    });
    return result;
  }

  private getRatesFromAPI() {
    const ratesName = 'rates';
    const baseName = 'base';
    this.executeHTTPCall().then(data=> {
      const apiRates = Object.getOwnPropertyNames(data[ratesName]);
      this.baseCurrency = data [baseName];
      apiRates.forEach (el => {
        this.rates.push(new Rate(el, data[ratesName][el]));
      });
    });
  }

  private executeHTTPCall() {
    return new Promise(resolve => {
      this.http.get(environment.apiUrl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
}
